using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public Transform projectileSpawnPoint;
    public GameObject projectilePrefab;
    public Transform rightHandMountPoint;
    public Transform leftHandMountPoint;
    public WeaponSlotType weaponSlot = WeaponSlotType.Pistol;
    public float projectileFireForce = 10f;
    public float fireCycleTime = 0.75f;
    private float fireCycleCountdown = 0f;

    public AudioSource fireSound;

    // Start is called before the first frame update
    void Start()
    {
        if (projectileSpawnPoint == null)
            projectileSpawnPoint = transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (fireCycleCountdown > 0)
            fireCycleCountdown -= Time.deltaTime;
    }

    public bool hitScan = false;
    public GameObject[] hitscanImpactPrefabs;

    public void Fire(Transform seekTarget = null)
    {
        if (hitScan || projectilePrefab == null)
        {
            //Treat this weapon as a projectile-less hit-scan weapon.

        }
        else
        {
            //Instantiate the projectitle and fire it.
            var round = Instantiate(projectilePrefab);
            var proj = round.GetComponentInChildren<Projectile>();
            var projRb = round.GetComponentInChildren<Rigidbody>();

            round.transform.SetPositionAndRotation(projectileSpawnPoint.position, projectileSpawnPoint.rotation);

            if (proj != null)
            {
                proj.seekTarget = seekTarget;
            }

            if (projRb != null)
            {
                projRb.AddForce(projectileSpawnPoint.forward * projectileFireForce);
            }

            fireCycleCountdown = fireCycleTime;
        }
        if (fireSound != null)
            fireSound.Play();
    }

    public bool CanFire()
    {
        return fireCycleCountdown <= 0f;
    }

    public enum WeaponSlotType
    {
        Pistol,
        Rifle,
        Bazooka
    }
}
