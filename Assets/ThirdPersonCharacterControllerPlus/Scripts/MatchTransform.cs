using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchTransform : MonoBehaviour
{
    public Transform transformToMatch;
    public bool matchPosition = true;
    public bool matchRotation = true;

    public Vector3 positionOffset = Vector3.zero;
    public Vector3 rotationOffset = Vector3.zero;
    private Vector3 oldRotationOffset = Vector3.one;
    private Quaternion rotationOffsetQuat;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (rotationOffset != oldRotationOffset)
        {
            rotationOffsetQuat = Quaternion.Euler(rotationOffset);
            oldRotationOffset = rotationOffset;
        }
        if (transformToMatch != null)
        {
            if (matchPosition)
                transform.position = transformToMatch.position + positionOffset;
            if (matchRotation)
                transform.rotation = transformToMatch.rotation * rotationOffsetQuat; //Quaternion.Euler(matchTransform.rotation.eulerAngles + rotationOffset);
        }
    }
}
