using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonPlusAnimationAudio : MonoBehaviour
{
    public AudioSource footStepSound;
    private float footStepCountdown = 0.1f;

    public AudioSource jumpSound;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (footStepCountdown > 0f)
            footStepCountdown -= Time.deltaTime;
    }


    public void OnFootStep()
    {
        if (footStepCountdown <= 0f)
        {
            footStepSound.Play();
            footStepCountdown = 0.2f;
        }
    }
    public void OnJump()
    {
        jumpSound?.Play();
    }
}
