using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Damageable : MonoBehaviour
{
    public float health = 100f;
    public float armor = 0f;
    public float armorDamageReduction = 0.5f;

    public GameObject[] activateOnDestroy;
    public GameObject[] deActivateOnDestroy;

    public ParticleSystem[] destroyParticles;
    public AudioSource[] destroyAudio;

    public float secondsToWaitBeforeCleanup = 0f;
    private float cleanupCountdown = 0f;
    private bool isDestroyed = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isDestroyed)
        {
            cleanupCountdown -= Time.deltaTime;
            if (ReadyToDestroy())
            {
                Destroy(gameObject);
            }
        }
    }

    private bool ReadyToDestroy()
    {
        return (cleanupCountdown <= 0f
            && destroyParticles?.Any(p => p.isPlaying) != true
            && destroyAudio?.Any(p => p.isPlaying) != true
        );
    }
    private void Destroy()
    {
        cleanupCountdown = secondsToWaitBeforeCleanup;
        isDestroyed = true;
        if (destroyParticles != null)
        {
            foreach (var part in destroyParticles)
                part.Play();
        }

        if (destroyAudio != null)
        {
            foreach (var audio in destroyAudio)
                audio.Play();
        }

        if (activateOnDestroy != null)
            foreach (var obj in activateOnDestroy)
                obj.SetActive(true);

        if (deActivateOnDestroy != null)
            foreach (var obj in deActivateOnDestroy)
                obj.SetActive(false);
    }

    public void TakeDamage(float damage)
    {
        if (armor > 0f)
        {
            armor -= damage;
            health -= damage * armorDamageReduction;
        }
        else
            health -= damage;

        if (health <= 0f)
            Destroy();
    }
}
