using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float impactDamage = 10f;

    Rigidbody rb;
    internal Transform seekTarget;

    public AudioSource[] onHitAudio;
    public ParticleSystem[] onHitParticles;

    public GameObject[] activateOnHit;
    public GameObject[] deactivateOnHit;

    public float lifeTime = 5f;
    private bool isDestroyed = false;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }
    // Start is called before the first frame update
    void Start()
    {
    }
    private void Update()
    {
        if (isDestroyed && CanDestroy())
        {
            Destroy(gameObject);
        }
        else
        {
            lifeTime -= Time.deltaTime;
            if (lifeTime <= 0f)
                isDestroyed = true;
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        Hit(collision.gameObject);
    }
    private void OnTriggerEnter(Collider other)
    {
        Hit(other.gameObject);
    }
    private bool CanDestroy()
    {
        return onHitAudio?.Any(a => a.isPlaying) != true
            && onHitParticles?.Any(p => p.isPlaying) != true;
    }
    private void Hit(GameObject hitObject)
    {
        isDestroyed = true;

        //Calculate the damage caused to the object that we hit.
        var damageable = hitObject.GetComponentInParent<Damageable>();
        if (damageable != null)
        {
            damageable.TakeDamage(impactDamage);
        }

        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;

        if (onHitAudio != null)
            foreach (var audio in onHitAudio)
                audio.Play();

        if (onHitParticles != null)
            foreach (var part in onHitParticles)
                part.Play();

        if (activateOnHit != null)
            foreach (var obj in activateOnHit)
                obj.SetActive(true);

        if (deactivateOnHit != null)
            foreach (var obj in deactivateOnHit)
                obj.SetActive(false);
    }
}
